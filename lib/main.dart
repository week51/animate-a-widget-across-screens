import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

void main() {
  runApp(HeroApp());
}

class HeroApp extends StatelessWidget {
  const HeroApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Transition Demo',
      home: MainScreen(),
    );
  }
}

class MainScreen extends StatelessWidget {
  const MainScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Main Screen'),
      ),
      body: GestureDetector(
        onTap: () {
          Navigator.push(context, MaterialPageRoute(builder: (context) {
            //return ออกเป็น widget
            return DetailScreen();
          }));
        },
        child: Hero(
          child: Image.network('https://picsum.photos/250?image=9'),
          tag: 'imagehero',
        ),
      ),
    );
  }
}

class DetailScreen extends StatelessWidget {
  const DetailScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        Navigator.pop(context);
      },
      //animation เปลี่ยนไป
      child: Center(
          child: Hero(
        child: Image.network('https://picsum.photos/250?image=9'),
        tag: 'imagehero',
      )),
    );
  }
}
